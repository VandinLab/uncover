Code for the paper:

Rebecca Sarto Basso, Dorit Hochbaum and Fabio Vandin. Efficient Algorithms to Discover Alterations with Complementary Functional Association in Cancer, RECOMB 2018.